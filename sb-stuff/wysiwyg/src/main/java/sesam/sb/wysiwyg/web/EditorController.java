package sesam.sb.wysiwyg.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EditorController {

    @GetMapping
    public String index(Model model) {
        model.addAttribute("title", "WYSIWYG - Editor");
        return "index";
    }
}
