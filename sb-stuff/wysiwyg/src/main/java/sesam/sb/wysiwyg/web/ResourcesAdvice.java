package sesam.sb.wysiwyg.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice(basePackages = "sesam.sb.wysiwyg.web")
public class ResourcesAdvice {

	@ModelAttribute("jspath")
	public String jsPath() {
		return "/wysiwyg.js?v=1.0";
	}

	@ModelAttribute("csspath")
	public String cssPath() {
		return "/wysiwyg.css?v=1.0";
	}
}
