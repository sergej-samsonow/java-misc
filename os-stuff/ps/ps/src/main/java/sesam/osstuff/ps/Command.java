package sesam.osstuff.ps;

import java.util.Optional;

public class Command {

    public static void main(String[] args) {
    	print("PID", "PARENT", "USER", "CMD");
		ProcessHandle.allProcesses().forEach(Command::printProcessInfo);
    }

    private static void print(String pid, String parentPid, String user, String cmd) {
    	System.out.println(String.format("%8s %8s %15s %s", pid, parentPid, user, cmd));
    }

	private static void printProcessInfo(ProcessHandle process) {
		print(token(Optional.of(process.pid())), token(process.parent().map(ProcessHandle::pid)), token(process.info().user()), token(process.info().commandLine()));
	}

	private static String token(Optional<?> optional) {
		return optional.map(Object::toString).orElse("-");
	}
}
