package sesam.sbssl.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sesam.sbssl.rest.schema.Info;

@RestController
@RequestMapping("/management")
public class InfoController {
	
	private final String version;
	
	public InfoController(@Value("${application.version}") String version) {
		this.version = version;
	}
	
	@GetMapping(path = "/info", produces = {MediaType.APPLICATION_JSON_VALUE})
	public Info info() {
		return new Info(version);
	}
	


}
