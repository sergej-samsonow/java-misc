package sesam.cmd.params;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ParamsTest {
    @Test
    @DisplayName("Parse one simple parameter")
    void simpleParameter() {
        Params params = Params.process(new String[]{"-n", "name"}, """
                My fancy app - single parameter
                
                    -n              User name
                """);
        assertThat(params.getParam("-n")).isEqualTo("name");
    }

    @Test
    @DisplayName("One short and long parameter")
    void oneShortAndLongParamert() {
        Params params = Params.process(new String[]{"-a", "25"}, """
                My fancy app - one short and long parameter
                
                    -a, --age=AGE   User age
                """);
        assertThat(params.getParam("-a", "--age")).isEqualTo("25");
        params = Params.process(new String[]{"--age=78"}, """
                My fancy app - simpleParameter
                
                    -a, --age=AGE   User age
                """);
        assertThat(params.getParam("-a", "--age")).isEqualTo("78");
    }

    @Test
    @DisplayName("Parse multiple parameters")
    void multipleParameters() {
        Params params = Params.process(new String[]{"-n", "name"}, """
                My fancy app - multiple parameters
                
                    -n              User name
                    -a, --age=AGE   User age
                """);
        assertThat(params.getParam("-n")).isEqualTo("name");
    }

    @Test
    @DisplayName("One but long parameter")
    void getOneButLongParamer() {
        Params params = Params.process(new String[]{"--name", "name"}, """
                My fancy app - one but long parameter
                
                --name          User name
                """);
        assertThat(params.getParam("--name")).isEqualTo("name");
    }

    @Test
    @DisplayName("Multiple parameters values")
    void getMultipleParameterValues() {
        Params params = Params.process(new String[]{"-n", "Sergej", "Julia"}, """
                My fancy app - multiple parameters values
                
                -n              Users names
                """);
        assertThat(params.getParams("-n")).contains("Sergej", "Julia");
    }

}