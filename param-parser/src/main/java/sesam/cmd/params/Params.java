package sesam.cmd.params;

import java.util.*;

public class Params {

    private String[] argv;
    private String description;
    private Map<String, String> definition;

    public Params(String[] argv, String description) {
        this.argv = argv;
        this.description = description;
        this.definition = new HashMap<>();
        parseDescription();
    }

    public static Params process(String argv[], String description) {
        return new Params(argv, description);
    }

    private void parseDescription() {
        boolean textFinished = false;
        for (String line : description.split("\\R")) {
            if (textFinished) {
                parseParamLine(line);
            }
            else if (line.isBlank()) {
                textFinished = true;
            }
        }
    }

    private void parseParamLine(String line) {
        String first = null;
        String second = null;
        for (String part : line.trim().split("\\s+")) {
            if (first == null) {
                first = part;
            }
            else if (second == null) {
                second = part;
                break;
            }
        }
        String shortParam = null;
        String longParam = null;
        if (first.startsWith("-")) {
            shortParam = first;
        }
        if (second.startsWith("--")) {
            shortParam = shortParam.replace(",", "");
            longParam = second;
            if (longParam.contains("=")) {
                String[] parts = longParam.split("=");
                longParam = parts[0];
            }
        }

        if (shortParam != null) {
            definition.put(shortParam, longParam == null ? "" : longParam);
        }
    }

    public String getParam(String param) {
        if (definition.containsKey(param)) {
            return searchForShortParam(param);
        }
        return null;
    }

    public String getParam(String simple, String complex) {
        String stored = definition.get(simple);
        if (complex.equals(stored)) {
            String found = searchForShortParam(simple);
            if (found != null) {
                return found;
            }
            return searchForComplexParam(complex);
        }
        return null;
    }

    private String searchForComplexParam(String complex) {
        for (String current : argv) {
            if (current.startsWith(complex)) {
                String[] parts = current.split("=");
                return parts[1];
            }
        }
        return null;
    }

    private String searchForShortParam(String param) {
        boolean found = false;
        for (String current : argv) {
            if (param.equals(current)) {
                found = true;
                continue;
            }
            if (found) {
                return current;
            }
        }
        return null;
    }

    public List<String> getParams(String param) {
        if (definition.containsKey(param)) {
            return searchForShortMultipleValues(param);
        }
        return null;
    }

    private List<String> searchForShortMultipleValues(String param) {
        boolean found = false;
        Set<String> params = definition.keySet();
        List<String> result = new ArrayList<>();
        for (String current : argv) {
            if (param.equals(current)) {
                found = true;
                continue;
            }
            else if (params.contains(current)) {
                break;
            }
            if (found) {
                result.add(current);
            }
        }
        return result;
    }

    public boolean hasParam(String key) {
        throw new RuntimeException("Not implemented yet");
    }

    public String getDescription() {
        throw new RuntimeException("Not implemented yet");
    }

}
