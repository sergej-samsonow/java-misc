package helloworld;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;

public class FPSPanel extends JPanel {

    private final transient FPSCalculator calculator;

    public FPSPanel() {
        this(new FPSCalculator());
    }

    public FPSPanel(FPSCalculator calculator) {
        super();
        this.calculator = calculator;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.BLACK);
        g.drawString("FPS: " + calculator.calculate(), 30, 30);
        repaint();
    }
}