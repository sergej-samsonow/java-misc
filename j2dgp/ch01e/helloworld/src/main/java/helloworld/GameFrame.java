package helloworld;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.HeadlessException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Container;

public class GameFrame extends JFrame implements Runnable {

    private final JPanel panel;

    public GameFrame(JPanel panel) throws HeadlessException {
        this.panel = panel;
    }

    @Override
    public void run() {
        panel.setBackground(Color.WHITE);
        panel.setPreferredSize(new Dimension(320, 240));

        Container container = getContentPane();
        container.add(panel);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Hello World");
        pack();
        setVisible(true);
    }
}