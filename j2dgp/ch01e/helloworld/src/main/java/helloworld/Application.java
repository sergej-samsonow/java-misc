package helloworld;

import javax.swing.SwingUtilities;

public class Application {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new GameFrame(new FPSPanel()));
    }
}
