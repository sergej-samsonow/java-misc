# Project sesam.project

- Application start 

```sh
java --module-path target/sesam.project-2023-12.1.jar --module sesam.project/sesam.project.Application
```

- JUL Configuration logging.properties

```properties
handlers=java.util.logging.ConsoleHandler
.level=ALL
java.util.logging.ConsoleHandler.level=ALL
java.util.logging.ConsoleHandler.formatter=java.util.logging.SimpleFormatter

# ALL FINEST FINER FINE CONFIG INFO WARNING SEVERE OFF
sesam.project.level=OFF

```

- Application start with logger

```sh
java -Djava.util.logging.config.file=logging.properties \
     --module-path target/sesam.project-2023-12.1.jar   \
     --module sesam.project/sesam.project.Application
```

- Java List Modules

```
java --list-modules
```
