package activerendering;


import javax.swing.JFrame;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.logging.Logger;

public class GameFrame extends JFrame implements Runnable {
    private static final Logger logger = Logger.getLogger(GameFrame.class.getName());

    private final transient GameLoop engine;
    private final transient FPSCalculator fps;
    private Canvas canvas;

    public GameFrame(GameLoop engine) {
        super();
        this.engine = engine;
        this.fps    = new FPSCalculator();
    }

    @Override
    public void run() {
        canvas = new Canvas();
        canvas.setSize(320, 240);
        canvas.setBackground(Color.BLACK);
        canvas.setIgnoreRepaint(true);
        getContentPane().add(canvas);

        setTitle("Active Rendering");
        setIgnoreRepaint(true);
        pack();
        setVisible(true);

        canvas.createBufferStrategy(2);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                logger.fine("Close window event received");
                engine.onWindowsClosing();
            }
        });
        engine.setGameFrame(this);
    }

    public BufferStrategy getStrategy() {
        return canvas.getBufferStrategy();
    }

    public void render(GameGraphics graphics) {
        Graphics awtGraphics = graphics.getAwtGraphics();
        awtGraphics.setColor(Color.GREEN);
        awtGraphics.drawString("FPS: " + fps.calculate(), 30, 30);
    }
}
