package activerendering;

public class FPSCalculator {

    private long lastTime;
    private long delta;
    private  long count;
    private long frameRate;

    public FPSCalculator() {
        lastTime = System.currentTimeMillis();
    }

    public long calculate() {
        long time = System.currentTimeMillis();
        delta += time - lastTime;
        lastTime = time;
        count ++;
        if (delta > 1000) {
            delta -= 1000;
            frameRate = count;
            count = 0;
        }
        return frameRate;
    }
}