package activerendering;

import java.awt.Graphics;

public class GameGraphics implements AutoCloseable {

    private final Graphics awtGraphics;
    private final int width;
    private final int height;

    public GameGraphics(Graphics graphics, int width, int height) {
        this.awtGraphics = graphics;
        this.width = width;
        this.height = height;
    }

    public void clear() {
        awtGraphics.clearRect(0, 0, width, height);
    }

    @Override
    public void close() {
        if (awtGraphics != null) {
            awtGraphics.dispose();
        }
    }

    public Graphics getAwtGraphics() {
        return awtGraphics;
    }
}
