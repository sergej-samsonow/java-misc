# Active Rendering Exmample with separate rendering Thread

### Current problems:

Introducing the `frame.dispose()` call causes strange application behavior 

1. Application window is closed
2. Application threads are in some "undefined" state. No other instructions are executed after the `frame.dispose()` call. Documentation says that JVM "may" terminate. However, the JVM is still running

Changes introduced that cause this behavior include:

**GameLoop.java**

```java
public void onWindowsClosing() {
    try {
        running = false;
        thread.join();                  // Step 1 
        System.exit(0);                 // Never reached after step 3
    }
    catch (InterruptedException e) {
        logger.warning("Interrupted exception on windows closing");
        Thread.currentThread().interrupt();
        System.exit(0);
    }
}
```

```java
private void gameLoop() {
    logger.fine("Start game loop");
    running = true;
    while (running) {
        // game logic
        render.render();
    }
    render.shutDown();                  // Step 2
    logger.fine("Finish game loop");    // Never reached after step 3
}
```

**BufferedRenderer.java** 

```java
public void shutDown() {
    frame.setVisible(false);
    frame.dispose();                    // Step 3
}
```

