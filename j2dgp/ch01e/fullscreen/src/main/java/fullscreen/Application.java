package fullscreen;

import javax.swing.SwingUtilities;

public class Application {
    public static void main(String[] args) throws Exception {
        // Main Game entry point
        GameLoop engine = new GameLoop();

        // Swing related stuff
        SwingUtilities.invokeLater(new GameFrame(engine));

        engine.startGameLoop();
    }
}