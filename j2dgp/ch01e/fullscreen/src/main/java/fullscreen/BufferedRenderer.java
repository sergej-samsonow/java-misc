package fullscreen;

import java.awt.image.BufferStrategy;

public class BufferedRenderer {

    private final GameFrame frame;
    private final BufferStrategy strategy;

    public BufferedRenderer(GameFrame frame) {
        this.frame = frame;
        this.strategy = frame.getStrategy();
    }

    private GameGraphics getGameGraphics() {
        return new GameGraphics(strategy.getDrawGraphics(), frame.getWidth(), frame.getHeight());
    }

    public void render() {
        do {
            do {
                try (GameGraphics graphics = getGameGraphics()) {
                    graphics.clear();
                    frame.render(graphics);
                }
            }
            while (strategy.contentsRestored());
            strategy.show();
        }
        while (strategy.contentsLost());
    }

    /**
     * Don't call this method until understand what happen after frame.dispose() call
     */
    public void shutDown() {
        frame.setVisible(false);
        // This call lead prevent JVM from shutdown properly
        frame.dispose();
    }
}
