package fullscreen;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

public class GameLoop {
    private static final Logger logger = Logger.getLogger(GameLoop.class.getName());

    private final CountDownLatch latch;
    private Thread thread;
    private boolean running;
    private BufferedRenderer render;

    public GameLoop() {
        running = false;
        latch = new CountDownLatch(1);
    }

    public void setGameFrame(GameFrame frame) {
        this.render = new BufferedRenderer(frame);
        latch.countDown();
    }

    public void startGameLoop() throws InterruptedException {
        latch.await();
        thread = new Thread(this::gameLoop);
        thread.start();
    }

    /**
     * Sonar think running never change to false and it is infinity loop
     */
    @SuppressWarnings({"java:S2589", "java:S2189"})
    private void gameLoop() {
        logger.fine("Start game loop");
        running = true;
        while (running) {
            // game logic
            render.render();
        }
        // render.shutDown() - disabled because leads to strange JVM behavior
        logger.fine("Finish game loop");
    }

    @SuppressWarnings("java:S112")
    public void onWindowsClosing() {
        try {
            running = false;
            thread.join();
            System.exit(0);
        }
        catch (InterruptedException e) {
            logger.warning("Interrupted exception on windows closing");
            Thread.currentThread().interrupt();
            System.exit(0);
        }
    }
}
