package fullscreen;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.logging.Logger;

public class GameFrame extends JFrame implements Runnable {
    private static final Logger logger = Logger.getLogger(GameFrame.class.getName());

    private final transient GameLoop engine;
    private final transient FPSCalculator fps;

    public GameFrame(GameLoop engine) {
        super();
        this.engine = engine;
        this.fps    = new FPSCalculator();
    }

    @Override
    public void run() {

        setIgnoreRepaint(true);
        setUndecorated(true);
        setBackground(Color.BLACK);

        var graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        var screenDevice = graphicsEnvironment.getDefaultScreenDevice();
        if ( ! screenDevice.isFullScreenSupported()) {
            logger.severe("Full screen mode not supported");
            System.exit(0);
        }
        screenDevice.setFullScreenWindow(this);

        createBufferStrategy(2);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if ( e.getKeyCode() == KeyEvent.VK_ESCAPE ) {
                    logger.fine("Close window event received");
                    engine.onWindowsClosing();
                }
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                logger.fine("Close window event received");
                engine.onWindowsClosing();
            }
        });
        engine.setGameFrame(this);
    }

    public BufferStrategy getStrategy() {
        return super.getBufferStrategy();
    }

    public void render(GameGraphics graphics) {
        Graphics awtGraphics = graphics.getAwtGraphics();
        awtGraphics.setColor(Color.GREEN);
        awtGraphics.drawString("FPS: " + fps.calculate(), 30, 30);
        awtGraphics.drawString("Press ESC to exit", 30, 60);
    }
}
