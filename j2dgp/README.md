# Programming kata project   

The goal of this project - is to practice programming with Java. 
The book by Timothy Wright (Fundamental 2D game programming with Java) is used as an 
inspirational resource.

[Original source code (book examples)](https://github.com/TimothyWrightSoftware/Fundamental-2D-Game-Programming-With-Java)

# Build and Run

Java 21 and Maven at least 3.9.1 required

```sh
mvn clean package
```

Run:

|Example                        |Command                                                                                                                    |
|:--                            |:--                                                                                                                        |
|Chapter 1 - Hello World        |`java --module-path ch01e/helloworld/target/helloworld.jar --module helloworld/helloworld.Application`                     |
|Chapter 1 - Active rendering   |`java --module-path ch01e/activerendering/target/activerendering.jar --module activerendering/activerendering.Application` |


